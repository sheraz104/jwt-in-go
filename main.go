package main

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

var (
	key = []byte("secret-key")
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", isAuthorized(handler))
	router.HandleFunc("/getToken", getToken)
	log.Fatal(http.ListenAndServe(":3000", router))
}

func getToken(w http.ResponseWriter, r *http.Request) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["authorized"] = true
	claims["client"] = "Sheraz"
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenStr, err := token.SignedString(key)
	if err != nil {
		http.Error(w, "error in signing", http.StatusInternalServerError)
		return
	}

	_, _ = w.Write([]byte(tokenStr))
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hell yeah")
}

func isAuthorized(endpoint func(w http.ResponseWriter, r *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			token := r.Header["Token"][0]

			tokenParsed, err := jwt.Parse(token, func(token *jwt.Token) (i interface{}, err error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf( "incorrect method")
				}
				return key, nil
			})

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			if !tokenParsed.Valid {
				http.Error(w, "token is invalid", http.StatusInternalServerError)
				return
			}

			endpoint(w, r)
		},
	)
}
